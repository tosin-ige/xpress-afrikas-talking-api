﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Xpress.AfricasTalking.Models
{
    public class ModelT
    {
    }

    public class beneficiaries
    {
        public string beneficiaryName { get; set; }
        public string amount { get; set; }
        public string beneficiaryAccountNumber { get; set; }
        public string beneficiaryBankCode { get; set; }
        public string narration { get; set; }
        public string referenceNumber { get; set; }
        public string responseCode { get; set; }
        public string responseDescription { get; set; }
    }
    public class PayXpressCallBackModel
    {
        public string scheduleId { get; set; }
        public string responseCode { get; set; }
        public string responseDescription { get; set; }
        public List<beneficiaries> beneficiaries { get; set; }
    }



}