﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AfricasTalkingCS;
using System.Configuration;
using System.Threading.Tasks;
using Xpress.AfricasTalking.Models;
using Newtonsoft.Json;
using System.Text;
using RestSharp;
using NLog;

namespace Xpress.AfricasTalking.Controllers
{
    public class GatewayController : ApiController
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static readonly HttpClient client = new HttpClient();

        [Route("api/v1/smsGateway")]
        public IHttpActionResult singleSMS(SMSRequest model) {
            // public static void sendSms([FromBody]SMSRequest model) { 
            try
            {
                var userName = ConfigurationManager.AppSettings["userName"];
                var apiKey = ConfigurationManager.AppSettings["apiKey"];
                var from = ConfigurationManager.AppSettings["from"];
                string inComingReq = JsonConvert.SerializeObject(model);
                logger.Info("\n------------------------------------------------------------------------------------------------------------------------\n"
                    + inComingReq);

                var req = JsonConvert.DeserializeObject<SMSRequest>(inComingReq);
               /* var requ = new SMSRequest
                {
                    to = req.to,
                    message = req.message
                };*/

                var receiver = req.to;          //"+2348055144000";
                var message = req.message;      //"Testing AfricasTalking API 1";                

                var gateway = new AfricasTalkingGateway(userName, apiKey);
                var sms = gateway.SendMessage(receiver, message, from);

                logger.Info("\n" + sms);

                return Ok(sms);
            }
            catch (AfricasTalkingGatewayException ex)
            {
                logger.Info(ex);
            }
                return Ok();
        }
                        
        // GET: api/Gateway
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Gateway/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Gateway
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Gateway/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Gateway/5
        public void Delete(int id)
        {
        }

        [Route("api/v1/jsonTest")]
        public async Task<IHttpActionResult> jsonTest([FromBody]PayXpressCallBackModel model)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri("https://localhost:44355");
                /*var req = new PayXpressCallBackModel()
                {
                    scheduleId = "xx",
                    responseCode = "xx",
                    responseDescription = "yd"

                };*/
                //StringContent content = new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"); //new StringContent(JsonConvert.SerializeObject(req), Encoding.UTF8, "application/json"));
                var response = client.PostAsync("/api/v1/jsonTest1", new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json")).Result;
                var responseContent = await response.Content.ReadAsStringAsync();
                dynamic json = JsonConvert.DeserializeObject<List<PayXpressCallBackModel>>(responseContent); //JsonConvert.DeserializeObject(responseContent);
                return Ok(json);
            }
        }



    }
}
